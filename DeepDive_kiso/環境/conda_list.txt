# packages in environment at C:\Users\deepdive_dl\.conda\envs\dl_kiso:
#
# Name                    Version                   Build  Channel
_tflow_select             2.3.0                       mkl  
absl-py                   0.8.0                    py37_0  
astor                     0.8.0                    py37_0  
attrs                     19.3.0                   pypi_0    pypi
backcall                  0.1.0                    pypi_0    pypi
blas                      1.0                         mkl  
bleach                    3.1.0                    pypi_0    pypi
ca-certificates           2019.8.28                     0  
certifi                   2019.9.11                py37_0  
colorama                  0.4.1                    pypi_0    pypi
cycler                    0.10.0                   py37_0  
decorator                 4.4.0                    pypi_0    pypi
defusedxml                0.6.0                    pypi_0    pypi
entrypoints               0.3                      pypi_0    pypi
freetype                  2.9.1                ha9979f8_1  
gast                      0.3.2                      py_0  
grpcio                    1.16.1           py37h351948d_1  
h5py                      2.9.0            py37h5e291fa_0  
hdf5                      1.10.4               h7ebc959_0  
icc_rt                    2019.0.0             h0cc432a_1  
icu                       58.2                 ha66f8fd_1  
importlib-metadata        0.23                     pypi_0    pypi
intel-openmp              2019.4                      245  
ipykernel                 5.1.2                    pypi_0    pypi
ipython                   7.8.0                    pypi_0    pypi
ipython-genutils          0.2.0                    pypi_0    pypi
ipywidgets                7.5.1                    pypi_0    pypi
jedi                      0.15.1                   pypi_0    pypi
jinja2                    2.10.3                   pypi_0    pypi
joblib                    0.13.2                   py37_0  
jpeg                      9b                   hb83a4c4_2  
jsonschema                3.1.1                    pypi_0    pypi
jupyter                   1.0.0                    pypi_0    pypi
jupyter-client            5.3.4                    pypi_0    pypi
jupyter-console           6.0.0                    pypi_0    pypi
jupyter-core              4.6.0                    pypi_0    pypi
keras                     2.2.4                         0  
keras-applications        1.0.8                      py_0  
keras-base                2.2.4                    py37_0  
keras-preprocessing       1.1.0                      py_1  
kiwisolver                1.1.0            py37ha925a31_0  
libmklml                  2019.0.5                      0  
libpng                    1.6.37               h2a8f88b_0  
libprotobuf               3.9.2                h7bd577a_0  
markdown                  3.1.1                    py37_0  
markupsafe                1.1.1                    pypi_0    pypi
matplotlib                3.1.1            py37hc8f65d3_0  
mistune                   0.8.4                    pypi_0    pypi
mkl                       2019.4                      245  
mkl-service               2.3.0            py37hb782905_0  
mkl_fft                   1.0.14           py37h14836fe_0  
mkl_random                1.1.0            py37h675688f_0  
more-itertools            7.2.0                    pypi_0    pypi
nbconvert                 5.6.0                    pypi_0    pypi
nbformat                  4.4.0                    pypi_0    pypi
notebook                  6.0.1                    pypi_0    pypi
numpy                     1.16.5           py37h19fb1c0_0  
numpy-base                1.16.5           py37hc3f5095_0  
openssl                   1.1.1d               he774522_2  
pandocfilters             1.4.2                    pypi_0    pypi
parso                     0.5.1                    pypi_0    pypi
pickleshare               0.7.5                    pypi_0    pypi
pip                       19.2.3                   py37_0  
prometheus-client         0.7.1                    pypi_0    pypi
prompt-toolkit            2.0.10                   pypi_0    pypi
protobuf                  3.9.2            py37h33f27b4_0  
pygments                  2.4.2                    pypi_0    pypi
pyparsing                 2.4.2                      py_0  
pyqt                      5.9.2            py37h6538335_2  
pyreadline                2.1                      py37_1  
pyrsistent                0.15.4                   pypi_0    pypi
python                    3.7.4                h5263a28_0  
python-dateutil           2.8.0                    py37_0  
pytz                      2019.3                     py_0  
pywin32                   225                      pypi_0    pypi
pywinpty                  0.5.5                    pypi_0    pypi
pyyaml                    5.1.2            py37he774522_0  
pyzmq                     18.1.0                   pypi_0    pypi
qt                        5.9.7            vc14h73c81de_0  
qtconsole                 4.5.5                    pypi_0    pypi
scikit-learn              0.21.3           py37h6288b17_0  
scipy                     1.3.1            py37h29ff71c_0  
send2trash                1.5.0                    pypi_0    pypi
setuptools                41.2.0                   py37_0  
sip                       4.19.8           py37h6538335_0  
six                       1.12.0                   py37_0  
sqlite                    3.29.0               he774522_0  
tensorboard               1.14.0           py37he3c9ec2_0  
tensorflow                1.14.0          mkl_py37h7908ca0_0  
tensorflow-base           1.14.0          mkl_py37ha978198_0  
tensorflow-estimator      1.14.0                     py_0  
termcolor                 1.1.0                    py37_1  
terminado                 0.8.2                    pypi_0    pypi
testpath                  0.4.2                    pypi_0    pypi
tornado                   6.0.3            py37he774522_0  
traitlets                 4.3.3                    pypi_0    pypi
vc                        14.1                 h0510ff6_4  
vs2015_runtime            14.16.27012          hf0eaf9b_0  
wcwidth                   0.1.7                    pypi_0    pypi
webencodings              0.5.1                    pypi_0    pypi
werkzeug                  0.16.0                     py_0  
wheel                     0.33.6                   py37_0  
widgetsnbextension        3.5.1                    pypi_0    pypi
wincertstore              0.2                      py37_0  
wrapt                     1.11.2           py37he774522_0  
yaml                      0.1.7                hc54c509_2  
zipp                      0.6.0                    pypi_0    pypi
zlib                      1.2.11               h62dcd97_3  

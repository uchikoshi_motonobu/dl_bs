import pickle
import numpy as np
from sklearn.model_selection import train_test_split
import os


np.random.seed(0)

def mldata():
    path_name=os.getcwd()+'\\mldata\\mnist.pickle'
    #print(path_name)
    with open(path_name,mode='rb') as fi:
        mnist=pickle.load(fi)

    n = len(mnist['data'])
    N = 10000  # MNISTの一部を使う
    indices = np.random.permutation(range(n))[:N]  # ランダムにN枚を選択

    X = mnist['data'][indices]
    y = mnist['target'][indices]
    Y = np.eye(10)[y.astype(int)]  # 1-of-K 表現に変換

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size=0.8)
    
    return X,Y, X_train, X_test, Y_train, Y_test

def mldata2():
    path_name=os.getcwd()+'\\mldata\\mnist.pickle'
    #print(path_name)
    with open(path_name,mode='rb') as fi:
        mnist=pickle.load(fi)

    n = len(mnist['data'])
    N = 30000  # MNISTの一部を使う
    N_train = 20000
    N_validation = 4000
    indices = np.random.permutation(range(n))[:N]  # ランダムにN枚を選択

    X = mnist['data'][indices]
    X = X / 255.0
    X = X - X.mean(axis=1).reshape(len(X), 1)
    y = mnist['target'][indices]
    Y = np.eye(10)[y.astype(int)]

    X_train, X_test, Y_train, Y_test = \
        train_test_split(X, Y, train_size=N_train)
    X_train, X_validation, Y_train, Y_validation = \
        train_test_split(X_train, Y_train, test_size=N_validation)
    
    return X,Y,y,X_train,X_test,Y_train,Y_test, X_validation, Y_train, Y_validation


if __name__ == '__main__':
    mldata()